import psycopg2
from psycopg2 import Error

def db_connect():
    try:
        connection = psycopg2.connect(
            user="postgres", password="uktvb100", host="127.0.0.1", port="5432"
        )
        cursor = connection.cursor()

        return cursor, connection
    except (Exception, Error) as error:
        print("Ошибка PostgreSQL", error)
    # finally:
    #     if connection:
    #         cursor.close()
    #         connection.close()
    #         print("Соединение с PostgreSQL закрыто")