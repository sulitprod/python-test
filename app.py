from flask import Flask, request, jsonify
from openpyxl import load_workbook
from datetime import datetime, date, time
from json import dumps

from db import db_connect

app = Flask(__name__)
db, connection = db_connect()

def dateSerial(data):
    print(type(data))
    if isinstance(data, (datetime, date, time)):
        return data.isoformat()

def addToDB(data):
    db.execute(
        "INSERT INTO hospitals (hospital_id, org_id, address) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING", 
        (data["hospital_id"], 0, data["address"])
    )
    connection.commit()
    db.execute(
        "INSERT INTO doctors (doctor_id, hospital_id, name) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING",
        (data["doctor_id"], data["hospital_id"], data["doctor_name"])
    )
    connection.commit()
    db.execute(
        "INSERT INTO tickets (ticket_id, doctor_id, ticket_date, begin_time, end_time, is_closed) VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING",
        (
            int("{0}{1}".format(data["doctor_id"], str(int(datetime.combine(data["ticket_date"], data["begin_time"]).timestamp()))[-5:])), 
            data["doctor_id"], 
            data["ticket_date"], 
            data["begin_time"], 
            data["end_time"], 
            data["is_closed"]
        )
    )
    connection.commit()

@app.route("/api/v1/organizations", methods=["GET"])
def GetMedicalOrganizations():
    db.execute("SELECT * FROM organizations")
    organizations = db.fetchall()

    return jsonify({ "request": "success", "response": organizations })

@app.route("/api/v1/tickets", methods=["GET"])
def GetTickets():
    args = request.args
    if "doctor_id" in args:
        db.execute("SELECT * FROM tickets WHERE doctor_id = {0}".format(args["doctor_id"]))
        tickets = db.fetchall()

        return dumps({ "request": "success", "response": tickets }, default=dateSerial)
    return jsonify({ "request": "error", "response": "Отсутствует параметр doctor_id" })

@app.route("/table_create")
def tableCreate():
    organizations = """CREATE TABLE organizations (org_id INT PRIMARY KEY, name VARCHAR);"""
    hospitals = """CREATE TABLE hospitals (hospital_id INT PRIMARY KEY, org_id INT, address VARCHAR);"""
    doctors = """CREATE TABLE doctors (doctor_id INT PRIMARY KEY, hospital_id INT, name VARCHAR);"""
    tickets = """CREATE TABLE tickets (ticket_id INT PRIMARY KEY, doctor_id INT, ticket_date DATE, begin_time TIME, end_time TIME, is_closed BOOL);"""
    db.execute(organizations)
    connection.commit()
    db.execute(hospitals)
    connection.commit()
    db.execute(doctors)
    connection.commit()
    db.execute(tickets)
    connection.commit()

    return "Tables created"

@app.route("/parse_table")
def parseTable():
    table = "test.xlsx"
    book = load_workbook(table)
    sheet = book.worksheets[0]

    for row in list(sheet.values)[1:]:
        if any(value for value in row):
            data = {
                "hospital_id": row[0],
                "address": row[1],
                "doctor_id": row[2],
                "doctor_name": row[3],
                "ticket_date": row[4],
                "begin_time": row[5],
                "end_time": row[6],
                "is_closed": row[7] == "занят"
            }
            addToDB(data)

    return "Scan completed"

app.run(debug=True)